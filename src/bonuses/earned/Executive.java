/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bonuses.earned;

/**
 *
 * @author Papa Momar
 */
public class Executive extends Employee {
    private float pctBonus;
    private float travelExpense;
    private float optionsCount;

    public Executive(float pctBonus, float travelExpense, float optionsCount, String name, float salary) {
        super(name, salary);
        this.pctBonus = pctBonus;
        this.travelExpense = travelExpense;
        this.optionsCount = optionsCount;
    }

    public float getPctBonus() {
        return pctBonus;
    }

    public float getTravelExpense() {
        return travelExpense;
    }

    @Override
    public float computeBonus() {
        float bonus=this.salary*this.pctBonus + 1000;
        return bonus;
    }

    public float getOptionsCount() {
        return optionsCount;
    }
   
}
