/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bonuses.earned;

/**
 *
 * @author macbookpro
 */
public class Worker extends Employee {
    
    protected float pctBonus;
    
    public Worker(String name, float salary, float pctBonus) {
        super(name, salary);
        this.pctBonus = pctBonus;
        
    }

    public float getPctBonus() {
        return pctBonus;
    }

    @Override
    public float computeBonus() {
        float bonus = salary * pctBonus;
        return bonus;
    }
    
}
