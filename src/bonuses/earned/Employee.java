package bonuses.earned;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author macbookpro
 */
public abstract class Employee {
    protected String name;
    protected float salary;

    public Employee(String name, float salary) {
        this.name = name;
        this.salary = salary;
    }
    
    public abstract float computeBonus ( );
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }
   
    
}
