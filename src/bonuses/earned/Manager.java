/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bonuses.earned;

/**
 *
 * @author Papa Momar
 */
public class Manager extends Employee{

    private float pctBonus;
    private float travelExpense;

    public float getPctBonus() {
        return pctBonus;
    }

    public float getTravelExpense() {
        return travelExpense;
    }

    public Manager(float pctBonus, float travelExpense, String name, float salary) {
        super(name, salary);
        this.pctBonus = pctBonus;
        this.travelExpense = travelExpense;
    }

    @Override
    public float computeBonus() {
        float bonus=this.salary*this.pctBonus + 500;
        return bonus;
    }

}
