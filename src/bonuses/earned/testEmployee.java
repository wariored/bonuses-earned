/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bonuses.earned;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author macbookpro
 */
public class testEmployee {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
           
        int nw, nm, ne, n;
        float salary, pctBonuses, travelExpense, bonus;
        String name, out, outW = null, outM = null, outE = null;
        Scanner enter = new Scanner(System.in);
        System.out.print("Enter the number of workers : ");
        nw = enter.nextInt();
        System.out.print("Enter the number of managers : ");
        nm = enter.nextInt();
        System.out.print("Enter the number of employee : ");
        ne = enter.nextInt();
        n = ne + nm + nw;
        Employee arrayEmployee[] = new Employee[n];
        for (int i = 0; i < nw; i++) {
            System.out.println("Enter the name of the worker " + (i+1));
            enter.nextLine();
            name = enter.nextLine();
            System.out.println("Enter the salary and bonus : ");
            salary = enter.nextFloat();
            pctBonuses = enter.nextFloat();
            Worker workerPerson = new Worker(name, salary, pctBonuses);
            arrayEmployee[i] = workerPerson;
            
        }
        for (int i = nw; i < nw + nm; i++) {
            System.out.println("Enter the name of the manager " + (i+1));
            enter.nextLine();
            name = enter.nextLine();
            System.out.print("Enter the salary and bonus : ");
            salary = enter.nextFloat();
            pctBonuses = enter.nextFloat();
            System.out.print("Enter the travel expense : ");
            travelExpense = enter.nextFloat();
            Manager managerPerson = new Manager(pctBonuses, travelExpense, name, salary);
            arrayEmployee[i] = managerPerson;
            
        }
        for (int i = nm + nw; i < n; i++) {
            float optionCount;
            System.out.println("Enter the name of the executive " + (i+1));
            enter.nextLine();
            name = enter.nextLine();
            System.out.print("Enter the salary and bonus : ");
            salary = enter.nextFloat();
            pctBonuses = enter.nextFloat();
            System.out.print("Enter the travel expense and option count : ");
            travelExpense = enter.nextFloat();
            optionCount = enter.nextFloat();
            Executive executivePerson = new Executive(pctBonuses, travelExpense, optionCount, name, salary);
            arrayEmployee[i] = executivePerson;
            
        }
   
        for (int i = 0; i < arrayEmployee.length; i++) {
            name = arrayEmployee[i].getName();
            salary = arrayEmployee[i].getSalary();
            bonus = arrayEmployee[i].computeBonus();
            
            if (arrayEmployee[i] instanceof Worker) {
                   outW = "Name: " + name + "\n";
                   outW= outW + "Salary: " + salary + "\n";
                   outW= outW + "Percentage Bonus: " + ((Worker)arrayEmployee[i]).getPctBonus() + "\n"; 
                   outW= outW + "Total Bonus: " + bonus + "\n\n";     
                }else if (arrayEmployee[i] instanceof Manager) {
                   outM = "Name: " + name + "\n";
                   outM= outM + "Salary: " + salary + "\n";
                   outM= outM + "Percentage Bonus: " + ((Manager)arrayEmployee[i]).getPctBonus() + "\n";
                   outM= outM + "Travel Expences: " + ((Manager)arrayEmployee[i]).getTravelExpense() + "\n";
                   outM= outM + "Total Bonus: " + bonus + "\n\n"; 
            }else if (arrayEmployee[i] instanceof Executive) {
                   outE = "Name: " + name + "\n";
                   outE= outE + "Salary: " + salary + "\n";
                   outE= outE + "Percentage Bonus: " + ((Executive)arrayEmployee[i]).getPctBonus() + "\n";
                   outE= outE + "Travel Expences: " + ((Executive)arrayEmployee[i]).getTravelExpense() + "\n";
                   outE= outE + "Option Count: " + ((Executive)arrayEmployee[i]).getOptionsCount() + "\n";
                   outE= outE + "Total Bonus: " + bonus + "\n\n"; 
                }
                
            }
        out = outW + outM + outE;
         JOptionPane.showMessageDialog(null, out, "Employee informations",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    

